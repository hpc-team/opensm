Source: opensm
Section: net
Priority: optional
Maintainer: Debian HPC Team <debian-hpc@lists.debian.org>
Uploaders: Mehdi Dogguy <mehdi@debian.org>
Build-Depends: bison,
               debhelper-compat (= 12),
               dpkg-dev (>= 1.13.19),
               flex,
               libibumad-dev (>= 1.3.7),
               libwrap0-dev,
               pkgconf,
               systemd-dev,
Standards-Version: 4.5.0
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/hpc-team/opensm.git
Vcs-Browser: https://salsa.debian.org/hpc-team/opensm
Homepage: https://github.com/linux-rdma/opensm

Package: opensm
Architecture: linux-any
Depends: infiniband-diags,
         libopensm9 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: InfiniBand subnet manager
 OpenSM provides an implementation of an InfiniBand Subnet Manager (SM) and
 Administrator (SA). One Subnet Manager is required to run on each InfiniBand
 subnet in order to initialize the InfiniBand hardware.

Package: opensm-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: Documentation for the InfiniBand subnet manager
 OpenSM provides an implementation of an InfiniBand Subnet Manager (SM) and
 Administrator (SA). One Subnet Manager is required to run on each InfiniBand
 subnet in order to initialize the InfiniBand hardware.
 .
 This package contains the documentation for the opensm InfiniBand subnet
 manager.

Package: libopensm9
Section: libs
Architecture: linux-any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: libopensm2, libopensm4, libopensm5
Replaces: libopensm2, libopensm4, libopensm5
Multi-Arch: same
Description: InfiniBand subnet manager library
 OpenSM provides an implementation of an InfiniBand Subnet Manager (SM) and
 Administrator (SA). One Subnet Manager is required to run on each InfiniBand
 subnet in order to initialize the InfiniBand hardware.

Package: libopensm-dev
Section: libdevel
Architecture: linux-any
Depends: libopensm9 (= ${binary:Version}),
         libosmcomp5 (= ${binary:Version}),
         libosmvendor5 (= ${binary:Version}),
         ${misc:Depends}
Conflicts: libopensm2-dev
Replaces: libopensm2-dev
Multi-Arch: same
Description: Header files for compiling against libopensm
 OpenSM provides an implementation of an InfiniBand Subnet Manager (SM) and
 Administrator (SA). One Subnet Manager is required to run on each InfiniBand
 subnet in order to initialize the InfiniBand hardware.
 .
 This package contains the header files for compiling applications
 against libopensm.

Package: libosmcomp5
Section: libs
Architecture: linux-any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: libopensm5 (<< 3.3.18-2~)
Replaces: libopensm5 (<< 3.3.18-2~)
Multi-Arch: same
Description: InfiniBand subnet manager - component library
 OpenSM provides an implementation of an InfiniBand Subnet Manager (SM) and
 Administrator (SA). One Subnet Manager is required to run on each InfiniBand
 subnet in order to initialize the InfiniBand hardware.

Package: libosmvendor5
Section: libs
Architecture: linux-any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: libopensm5 (<< 3.3.18-2~)
Replaces: libopensm5 (<< 3.3.18-2~)
Multi-Arch: same
Description: InfiniBand subnet manager - vendor library
 OpenSM provides an implementation of an InfiniBand Subnet Manager (SM) and
 Administrator (SA). One Subnet Manager is required to run on each InfiniBand
 subnet in order to initialize the InfiniBand hardware.
